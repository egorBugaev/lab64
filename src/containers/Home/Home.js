import React, {Component} from 'react'
import axios from 'axios'
import Spinner from "../../components/UI/Spinner/Spinner";
import Posts from "../../components/Posts/Posts";

class Home extends Component {
	
	state = {
		posts: [],
		loading: false
	};
	
	updatePosts = async () => {
		this.setState({loading: true});
		let posts = await axios.get('/posts.json').finally(() => {
			this.setState({loading: false});
		});
		let newPosts = [];
		for (let key in posts.data) {
			newPosts.push({
				title: posts.data[key].title,
				id: key,
				date: posts.data[key].date,
				text: posts.data[key].text,
				disabled: posts.data[key].disabled
			});
		}
		this.setState({posts: newPosts});
	};
	
	componentDidMount() {
		this.updatePosts();
	}
	
	getFullPost = (id) => {
		let posts = [...this.state.posts];
		const index = posts.findIndex(p => p.id === id);
		const queryParams = [];
		for(let key in this.state.posts[index]) {
			queryParams.push(
				encodeURIComponent(key) + '=' + encodeURIComponent(this.state.posts[index][key])
			);
		}
		const queryString = queryParams.join('&');
		this.props.history.push({
			pathname: '/posts/:id',
			search: '?' + queryString
		})
	};
	
	render() {
		return this.state.loading ? <Spinner/> : <Posts	readMore={this.getFullPost}	posts={this.state.posts}/>;
	}
}

export default Home;