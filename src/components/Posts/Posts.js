import React from 'react'
import Post from "./Post/Post";

const Posts = props => {
	return (
		<ul>
			{
				props.posts.map(post => {
					return (
						<Post
							key={post.id}
							date={post.date}
							title={post.title}
							text={post.text}
							readMore={() => props.readMore(post.id)}
						/>
					)
				})
			}
		</ul>
	)
};

export default Posts;