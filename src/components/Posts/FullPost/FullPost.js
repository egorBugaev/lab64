import React, {Component} from 'react'
import './FullPost.css'
import Moment from "react-moment";
import axios from 'axios'

class FullPost extends Component {
	
	state = {
		post: {}
	};
	
	componentDidMount() {
		const query = new URLSearchParams(this.props.location.search);
		const post = {};
		for (let param of query.entries()) {
			post[param[0]] = param[1];
		}
		this.setState({post});
	}
	
	deletePostHandler = () => {
		axios.delete(`/posts/${this.state.post.id}.json`).finally(() => {
			this.props.history.replace('/posts');
		});
	};
	
	editPostTextHandler = () => {
		let post = {...this.state.post};
		const queryParams = [];
		for (let key in post) {
			queryParams.push(
				encodeURIComponent(key) + '=' + encodeURIComponent(post[key])
			);
		}
		const queryString = queryParams.join('&');
		this.props.history.push({
			pathname: '/posts/' + post.id + '/edit',
			search: '?' + queryString
		})
	};
	
	render() {
		return (
			<div className="FullPost">
				<div className="FullPost__date">Created on: <Moment format="DD-MM-YY HH:mm"
				                                                    date={this.state.post.date}/></div>
				<div className="FullPost__title">Title: {this.state.post.title}</div>
				<div className="FullPost__text">{this.state.post.text}</div>
				<div className="FullPost__buttons">
					<button onClick={this.deletePostHandler} className="FullPost__delete">Delete</button>
					<button onClick={this.editPostTextHandler} className="FullPost__edit">Edit</button>
				</div>
			</div>
		)
	}
}

export default FullPost;